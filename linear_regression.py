# this function implements gradient decent as discussed in the blog
#it takes in an array of values which are to be fitted to the curve
#and returns a pair of slope and decent

def cumulative_error(estimate,real):
    #calculates the error and returns an int
    return sum(estimate-real)**2/count(estimate)

def linear_regression_gd(set_of_values,acceleration=0.5,accuracy=0.05):
    while(diff>accuracy):
        #estimate the error for starting
        cumulative_error(set_of_values,slope,)
        #estimate the slope for error
        slope0=slope
        slope=slope-error*acceleration
        #check if accuracy is reached
        accuracy=(slope0-slope)/slope0
        slope0=slope
        
    mean_x=sum(set_of_values)/count(set_of_values)
    mean_y=sum(set_of_values_y)/count(set_of_values_y)
    intercept=mean_x*slope-mean_y
    return intercept,slope
